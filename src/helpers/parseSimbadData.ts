
export default function parseSimbadData (data: string): Map<string, string | string[]> {
    const startIndex = data.indexOf('Coordinates');
    const endIndex = data.indexOf('Notes (0) :');
    const dataParts = data.slice(startIndex, endIndex).split('\n');
    const result = new Map<string, string | string[]>();
    let currentArray = '';
    for(let i = 0; i < dataParts.length; i++) {
        if (dataParts[i].length === 0) {
            continue;
        }
        const keyAndValue = dataParts[i].split(':');
        if (keyAndValue.length === 1) { // is part of previous iteration data'
            const values = keyAndValue[0].split('  ').filter((value) => {
                if (value.trim() !== '') {
                    return value;
                }
            });
            const previousValue = result.get(currentArray); // part should be added to previous iteration data
            let sumValue;
            if(!Array.isArray(previousValue)){
                sumValue = values;
            } else {
                sumValue = previousValue.concat(values);
            }
            result.set(currentArray, sumValue);
            continue;
        }
        const dataAndElementCount = keyAndValue[0].split(' ');
        const count = dataAndElementCount[dataAndElementCount.length -1].slice(1, -1);
        if (!isNaN(+count) && !isNaN(parseInt(count))) { // we deal with array, such as identifiers or bibcodes
            result.set(dataAndElementCount[0], '');
            currentArray = dataAndElementCount[0];
            continue;
        }
        result.set(keyAndValue[0], keyAndValue[1]);
    }
    const objectType = data.split(' --- ')[1];
    result.set('objectType', objectType);
    return result;
}
