import { ServiceConfig } from 'package-app';

export const config: ServiceConfig = {
    name: 'simbad',
    brokerConfig: {
        nodeID: 'simbad',
        transporter: 'redis://localhost:6379',
        logger: true,
        logLevel: 'info'
    }
}
export default config;
