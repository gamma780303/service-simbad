export interface SimbadObjectData {
    CoordinatesICRS: string;
    CoordinatesFK4: string;
    CoordinatesGal: string;
    Parallax: string;
    RadialVelocity: string;
    Redshift: string;
    Identifiers: string[];
    SpectralType: string;
    Fluxes: string[];
}
