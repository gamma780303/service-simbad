import {Action, App, Payload} from 'package-app';
import {
    ArticleActionName,
    GetObjectByIdPayload, GetObjectByIdResult,
    SearchArticlesPayload,
    SearchArticlesResult,
    ServiceName,
    SimbadActionName
} from "package-types";
import axios from "axios";
import parseSimbadData from "../helpers/parseSimbadData";

export default new class GetObjectById implements Action{
    getName(): string{
        return SimbadActionName.GetObjectById;
    }

    getValidationSchema(): any {
        return {};
    }

    async execute(payload: Payload<GetObjectByIdPayload>): Promise<GetObjectByIdResult> {
        const { identifier } = payload.params;
        const {data} = await axios.get(`http://simbad.u-strasbg.fr/simbad/sim-id?output.format=ASCII&obj.bibsel=off&Ident=${identifier}`);
        const parsed = parseSimbadData(data);
        const { articles } = await App.call<SearchArticlesPayload, SearchArticlesResult>(ServiceName.Article, ArticleActionName.GetArticles, {
            text: `%${identifier}`
        });
        return {object: {...Object.fromEntries(parsed)}, articles}
    }
}





